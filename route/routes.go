package routes

import (
	"myapp/controller"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func InitializaRouters() {
	// Creating a router
	router := mux.NewRouter()

	// registering route and mapping the handler function
	router.HandleFunc("/home",controller.HomeHandler) // string type and handler type as argument
	// route of type string as aregument and handler func

	router.HandleFunc("/urlParameter/{myname}",controller.ParameterHandler)
	

// student 
	router.HandleFunc("/student", controller.AddStudent).Methods("POST")

// to get the particular sid data from database 
	router.HandleFunc("/student/{sid}",controller.GetStud).Methods("GET")

	// update
	router.HandleFunc("/student/{sid}",controller.UpdateStud).Methods("PUT")

	// delete
	router.HandleFunc("/student/{sid}",controller.DeleteStud).Methods("DELETE")


	router.HandleFunc("/students",controller.GetAllStudents)

	// Path to serve the static files 
	// if this is not there, it won't show any view phase
	// returns a Handler object and takes an argument of FileSystem interface type.	
	// implements Open method defined by the FileSystem interface
	// Dir for type casting into string data type
	fhandler := http.FileServer(http.Dir("./View"))
	router.PathPrefix("/").Handler(fhandler)

// start the server in port 8080
	err := http.ListenAndServe(":8080", router) //local host port
	if err != nil {
		os.Exit(1)
	}
}